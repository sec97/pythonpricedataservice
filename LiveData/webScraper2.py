import lxml as lxml
from numpy.core.fromnumeric import repeat
import requests
from bs4 import BeautifulSoup
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import numpy as np


HSI = ['FB', 'TSLA', 'ORCL', 'UBER']


def real_time_price(stock_code):
    url = ('https://finance.yahoo.com/quote/' + stock_code +
           '?p=' + stock_code + '&.tsrc=fin-srch')
    r = requests.get(url)
    web_content = BeautifulSoup(r.text, 'lxml')
    web_content = web_content.find(
        'div', {"class": 'My(6px) Pos(r) smartphone_Mt(6px)'})
    web_content = web_content.find('span').text
    if web_content == []:
        web_content = '99999999999'
    return web_content


# style.use('fivethirtyeight')
fig = plt.figure()
fig.subplots_adjust(wspace=1.0, hspace=1.0)

ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 2)
ax3 = fig.add_subplot(2, 2, 3)
ax4 = fig.add_subplot(2, 2, 4)

df = pd.DataFrame(columns=['date',*HSI])

def animate(i):
    global df
    price = []
    col = []
    time_stamp = datetime.datetime.now()
    time_stamp = time_stamp.strftime("%Y-%m-%d-%H:%M:%S")
    for stock_code in HSI:
        price.append(real_time_price(stock_code))
    col = [time_stamp]
    col.extend(price)
    df = df.append(pd.Series(col, index=df.columns, name=1), ignore_index=True)

    ys = df.iloc[i:, 1].values
    xs = df.iloc[i:,0].values

    ax1.plot(xs, ys, marker='o', color='b')
    ax1.set_title('Facebook Stock Price', fontsize=12)

    ys = df.iloc[i:, 2].values
    ax2.plot(xs, ys, marker='o', color='gray')
    ax2.set_title('Tesla Stock Price', fontsize=12)

    ys = df.iloc[i:, 3].values
    ax3.plot(xs, ys, marker='o', color='firebrick')
    ax3.set_title('Oracle Stock Price', fontsize=12)

    ys = df.iloc[i:, 4].values
    ax4.plot(xs, ys, marker='o', color='k')
    ax4.set_title('Uber Stock Price', fontsize=12)


ani = animation.FuncAnimation(fig, animate, frames=10, interval=100, repeat=False)

plt.tight_layout()
plt.show()